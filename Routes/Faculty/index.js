const express = require('express');
const router = express.Router();
const mongo = require('../../mongo');





router.post('/course_name', async (req, res) => {
    let name = []; batch_detail = [];

    let db1 = mongo.get().collection("subject_table")
    let db2 = mongo.get().collection("batch_table")
    let Course_name = await db1.find().toArray()

    for (let i = 0; i < Course_name.length; i++) {
        name.push(Course_name[i].course_name)

    }
    let Batch_data = await db2.find({ faculty_id: req.body.faculty_id }).toArray()

    for (let i = 0; i < Batch_data.length; i++) {
        batch_detail.push(Batch_data[i].batch_name)

    }

    console.log(batch_detail);
    res.json({ Course_name: name, Batch_data: Batch_data, batch_detail: batch_detail });

});

router.post('/course_code', async (req, res) => {


    let db1 = mongo.get().collection("subject_table")
    let Course_code = await db1.find({ "course_name": req.body.course_code }).toArray()

    res.json({ Course_code: Course_code[0].course_code });

});

router.post('/create_assignment', async (req, res) => {

    let db1 = mongo.get().collection("question_table")
    let new_id = 1;
    let max = await db1.find().sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    let json = {
        "_id": parseInt(new_id),
        "faculty_id": req.body.faculty_id,
        "faculty_name": req.body.faculty_name,
        "batch_name": req.body.batch_name,
        "batch_id": parseInt(req.body.batch_id),
        "course_name": req.body.course_name.toUpperCase(),
        "course_code": req.body.course_code,
        "assignment_name": req.body.assignment_name.toUpperCase(),
        "open_date": req.body.open_date,
        "deadline_date": req.body.deadline_date,
        "content": req.body.content
    }
    let insert = await db1.insertOne(json);
    if (insert.acknowledged) {
        res.json({ status: 1 });
    }
    else {
        res.json({ status: 0 });
    }

});

router.post('/create_batch', async (req, res) => {

    let db1 = mongo.get().collection("batch_table")
    let new_id = 1;
    let max = await db1.find().sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    let json = {
        "_id": parseInt(new_id),
        batch_name: req.body.batch_name.toUpperCase(),
        "faculty_id": req.body.faculty_id,
        "faculty_name": req.body.faculty_name,
        "course_name": req.body.course_name.toUpperCase(),
        "course_code": req.body.course_code,

    }
    console.log(json)
    let insert = await db1.insertOne(json);
    let Batch_data = await db1.find({ faculty_id: req.body.faculty_id }).toArray()



    if (insert.acknowledged) {
        res.json({ status: 1, Batch_data: Batch_data });
    }
    else {
        res.json({ status: 0 });
    }

});

router.post('/batch_data', async (req, res) => {

    let db1 = mongo.get().collection("user_table")
    let Batch_course = req.body.Batch_course;


    let data = await db1.find({ "role": "student", "slot": { $elemMatch: { "course_code": Batch_course } } }).toArray()
    res.json({ data: data });
});

router.post('/submission_details', async (req, res) => {

    let db1 = mongo.get().collection("submission_table")
    let details = await db1.find({ faculty_id: parseInt(req.body._id) }).toArray()
    res.json({ details: details });
});

router.post('/question_view', async (req, res) => {
    let questionarray = [];
    let db1 = mongo.get().collection("question_table")
    let question = await db1.find({ _id: parseInt(req.body.question_id) }).toArray()

    for (let i = 0; i < question.length; i++) {
        questionarray.push(question[i].content)

    }
    console.log(questionarray)
    res.json({ question: questionarray });

});

router.post('/accept', async (req, res) => {

    let db1 = mongo.get().collection("submission_table")
    let update = db1.updateOne({ '_id': parseInt(req.body._id) }, { $set: { status: 2, mark: parseInt(req.body.mark), msg: "empty" } })

    res.json({ status: 1 })
});

router.post('/reject', async (req, res) => {

    let db1 = mongo.get().collection("submission_table")
    let update = db1.updateOne({ '_id': parseInt(req.body._id) }, { $set: { status: 3, msg: req.body.msg } })

    res.json({ status: 1 })
});
router.post('/batch_id', async (req, res) => {

    let db1 = mongo.get().collection("batch_table")

    let Batch_id = await db1.find({ batch_name: req.body.batch_name }).toArray()

    res.json({ Batch_id: Batch_id[0]._id })
});

router.post('/dashboard_count', async (req, res) => {
    var status = [];

    let db1 = mongo.get().collection("batch_table")
    let db2 = mongo.get().collection("submission_table")
    let db3 = mongo.get().collection("question_table");

    let Batch_data = await db1.find({ faculty_id: req.body.faculty_id }).toArray()
    let submission_data = await db2.find({ faculty_id: req.body.faculty_id }).toArray()

    for (let i = 0; i < Batch_data.length; i++) {
        let batch_id = parseInt(Batch_data[i]._id);
        question_count = await db3.find({ "batch_id": parseInt(batch_id) }).count();
        let submission = await db3.find({ "batch_id": parseInt(batch_id) }).toArray();
        not_submission_count = question_count - submission.length;

        let batch_name = Batch_data[i].batch_name;
        let course_code = Batch_data[i].course_code;
        let course_name = Batch_data[i].course_name;

        let submission_array = submission_data.filter(function (obj) {
            return obj.batch_id === batch_id;
        });

        let submission_count = submission_array.length;
        let accepted_array = submission_array.filter(function (obj) {
            return obj.status === 2;
        });
        let accepted_count = accepted_array.length;

        let rejected_array = submission_array.filter(function (obj) {
            return obj.status === 3;
        });
        let rejected_count = rejected_array.length;

        let json = {
            batch_id: batch_id,
            batch_name: batch_name,
            course_code: course_code,
            course_name: course_name,
            submission_count: submission_count,
            accepted_count: accepted_count,
            rejected_count: rejected_count,
            question_count: question_count,
            not_submission_count: not_submission_count,
        }

        status.push(json);
    }

    console.log(status)
    res.json({ Batch_data: status })
});
module.exports = router;