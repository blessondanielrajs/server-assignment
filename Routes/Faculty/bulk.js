const express = require('express');
const router = express.Router();
const multer = require('multer'); //Its use for uploading
const path = require('path');
const fs = require('fs');
const MD5 = require('crypto-js/md5');//secure hash funtions Its use for password
var parse = require('csv-parse');// Excel format Use for bulk uploading
const mongo = require('../../mongo');
//image storage(documents)
const imageStorage = multer.diskStorage({
    destination: 'temp/',
    destination: async function (req, file, cb) {
        let path = 'temp/';
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path, { recursive: true }, (err) => {
                if (err) throw err;
            });
        }
        await cb(null, path);
    },
    filename: (req, file, cb) => {
        cb(null, "file" + path.extname(file.originalname))
    }

});

const imageUpload = multer({
    storage: imageStorage,
    limits: {
        fileSize: 1000000 // 1000000 Bytes = 1 MB
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(csv)$/)) {
            return cb(new Error('Please upload a CSV'))
        }
        cb(undefined, true);
    }
})



router.post('/', imageUpload.single('file'), async (req, res) => {
    var csvData = []; var status = []

    let path = "temp/file.csv";
    let db1 = mongo.get().collection("user_table")

    await fs.createReadStream(path)
        .pipe(parse({ delimiter: ',' }))
        .on('data', async function (csvrow) {
            csvData.push(csvrow);
        })
        .on('end', async function () {

            for (let i = 1; i < csvData.length; i++) {
                let emptyjson = { "batch_id": parseInt(req.body.batch_id), "course_code": req.body.course_code };

                let query = await db1.find({ '_id': parseInt(csvData[i][1]) }).toArray();
               let oldslot = query[0].slot;
                if (oldslot.length === 0) {
                    // Slot Array Empty
                    finalslot = {
                        slot: [emptyjson]
                    };
                }
                else {
                    //1.Slot Array not Empty
                    //2.If course code available
                    //3. Course Code not available
                    oldslot = oldslot.filter(item => item.course_code !== req.body.course_code);//remove that json
                    oldslot.push(emptyjson);

                    finalslot = {
                        slot: oldslot
                    }
                }

                update = db1.updateOne({ '_id': parseInt(csvData[i][1]) }, { $set: finalslot })
                status.push({ json: csvData[i][1], addstatus: 1, msg: "Successfully Added", });

            }


            res.json({ status: 1, addstatus: status })
        });

}, (error, req, res, next) => {
    res.status(400).send({ error: error.message });
});
module.exports = router;