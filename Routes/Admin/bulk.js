const express = require('express');
const router = express.Router();
const multer = require('multer'); //Its use for uploading
const path = require('path');
const fs = require('fs');
const MD5 = require('crypto-js/md5');//secure hash funtions Its use for password
var parse = require('csv-parse');// Excel format Use for bulk uploading
const mongo = require('../../mongo');
//image storage(documents)
const imageStorage = multer.diskStorage({
    destination: 'temp/',
    destination: async function (req, file, cb) {
        let path = 'temp/';
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path, { recursive: true }, (err) => {
                if (err) throw err;
            });
        }
        await cb(null, path);
    },
    filename: (req, file, cb) => {
        cb(null, "file" + path.extname(file.originalname))
    }

});

const imageUpload = multer({
    storage: imageStorage,
    limits: {
        fileSize: 1000000 // 1000000 Bytes = 1 MB
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(csv)$/)) {
            return cb(new Error('Please upload a CSV'))
        }
        cb(undefined, true);
    }
})



router.post('/faculty', imageUpload.single('file'), async (req, res) => {
    var csvData = []; let status = [], key = 0;
    let path = "temp/file.csv";
    let db1 = mongo.get().collection("user_table")
    let max = await db1.find({ "role": "faculty" }).sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    if (max.length !== 0) {
        max = parseInt(max[0]._id);
        max = max + 1;
    }
    else {
        max = 101;
    }
    await fs.createReadStream(path)
        .pipe(parse({ delimiter: ',' }))
        .on('data', async function (csvrow) {
            csvData.push(csvrow);
        })
        .on('end', async function () {
            for (let i = 1; i < csvData.length; i++) {
                let new_id = parseInt(max);
                let password ="faculty";
               
                let json = {
                    "_id": new_id,
                    "key": new_id,
                    "faculty_name": csvData[i][1].toUpperCase().trim(),
                    "role": csvData[i][2].trim(),
                    "password": MD5(password).toString()
                    }
                
            var pattern_password = /^[A-Za-z]\w{6,14}$/;
             
                if (json.faculty_name === "") {
                    status.push({ json: json.name, addstatus: 0, msg: "Enter Name", "key": ++key });
                }
                else if (json.role === "") {
                    status.push({ json: json.role, addstatus: 0, msg: "Enter Role", "key": ++key });
                }
                else if (password === "" || !pattern_password.test(password)) {
                    status.push({ json: password, addstatus: 0, msg: "Invalid Password", "key": ++key });
                }
                else {
                    let user = await db1.insertOne(json);
                    if (user.acknowledged) {
                        max = max + 1;
                        status.push({ json: json.faculty_name, addstatus: 1, msg: "Successfully Added", "key": ++key });
                    }
                }
            }
            res.json({ Status: 1, addStatus: status });
        });

}, (error, req, res, next) => {
    res.status(400).send({ error: error.message });
});

router.post('/student', imageUpload.single('file'), async (req, res) => {
    var csvData = []; let status = [], key = 0;
    let path = "temp/file.csv";
    let db1 = mongo.get().collection("user_table")
    let max = await db1.find({ "role": "student" }).sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    if (max.length !== 0) {
        max = parseInt(max[0]._id);
        max = max + 1;
    }
    else {
        max = 1001;
    }
    await fs.createReadStream(path)
        .pipe(parse({ delimiter: ',' }))
        .on('data', async function (csvrow) {
            csvData.push(csvrow);
        })
        .on('end', async function () {
            for (let i = 1; i < csvData.length; i++) {
                let new_id = parseInt(max);
                let password = "student";

                let json = {
                    "_id": new_id,
                    "key": new_id,
                    "student_name": csvData[i][1].toUpperCase().trim(),
                    "role": csvData[i][2].trim(),
                    "department": csvData[i][3].toUpperCase().trim(),
                    "slot":[],
                    "password": MD5(password).toString()
                }

                var pattern_password = /^[A-Za-z]\w{6,14}$/;

                if (json.faculty_name === "") {
                    status.push({ json: json.name, addstatus: 0, msg: "Enter Name", "key": ++key });
                }
                else if (json.role === "") {
                    status.push({ json: json.role, addstatus: 0, msg: "Enter Role", "key": ++key });
                }
                else if (password === "" || !pattern_password.test(password)) {
                    status.push({ json: password, addstatus: 0, msg: "Invalid Password", "key": ++key });
                }
                else {
                    let user = await db1.insertOne(json);
                    if (user.acknowledged) {
                        max = max + 1;
                        status.push({ json: json.student_name, addstatus: 1, msg: "Successfully Added", "key": ++key });
                    }
                }
            }
            res.json({ Status: 1, addStatus: status });
        });

}, (error, req, res, next) => {
    res.status(400).send({ error: error.message });
});

module.exports = router;