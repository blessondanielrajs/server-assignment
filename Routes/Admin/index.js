const express = require('express');
const router = express.Router();
const mongo = require('../../mongo');
const MD5 = require('crypto-js/md5');


router.post('/create_course', async (req, res) => {

    let db1 = mongo.get().collection("subject_table")

    let json = {
        "_id": req.body.course_code.toUpperCase(),
        "course_name": req.body.course_name.toUpperCase(),
        "course_code": req.body.course_code.toUpperCase(),
    }
    let insert = await db1.insertOne(json);
    let Courses = await db1.find().toArray()

    if (insert.acknowledged) {
        res.json({ status: 1, courses: Courses });
    }
    else {
        res.json({ status: 0 });
    }


});

router.post('/courses', async (req, res) => {

    let db1 = mongo.get().collection("subject_table")
    let Courses = await db1.find().toArray()

    res.json({ courses: Courses });

});


router.post('/single_faculty', async (req, res) => {

    let db1 = mongo.get().collection("user_table")
    let PASSWORD = MD5("faculty").toString();
    let new_id = 101;
    let max = await db1.find({ "role": "faculty" }).sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    let json = {
        "_id": new_id,
        "key": new_id,
        "faculty_name": req.body.name.toUpperCase().trim(),
        "role": "faculty",
        "password": PASSWORD
    }

    let insert = await db1.insertOne(json);
    if (insert.acknowledged) {
        res.json({ status: 1 });
    }
    else {
        res.json({ status: 0 });
    }

});

router.post('/single_student', async (req, res) => {

    let db1 = mongo.get().collection("user_table")
    let new_id = 1001;
    let PASSWORD = MD5("student").toString();
    let max = await db1.find({ "role": "student" }).sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    let json = {
        "_id": new_id,
        "key": new_id,
        "student_name": req.body.name.toUpperCase().trim(),
        "role": "student",
        "department": req.body.department.toUpperCase().trim(),
        "slot": [],
        "password": PASSWORD
    }
    let insert = await db1.insertOne(json);
    if (insert.acknowledged) {
        res.json({ status: 1 });
    }
    else {
        res.json({ status: 0 });
    }

});


router.post('/dashboard_count', async (req, res) => {
    var status = [];

    let db1 = mongo.get().collection("batch_table")
    let db2 = mongo.get().collection("submission_table")
    let db3 = mongo.get().collection("question_table");

    let Batch_data = await db1.find().toArray()
    let submission_data = await db2.find().toArray()

    for (let i = 0; i < Batch_data.length; i++) {
        let batch_id = parseInt(Batch_data[i]._id);
        question_count = await db3.find({ "batch_id": parseInt(batch_id) }).count();
        let submission = await db3.find({ "batch_id": parseInt(batch_id) }).toArray();
        not_submission_count = question_count - submission.length;

        let batch_name = Batch_data[i].batch_name;
        let course_code = Batch_data[i].course_code;
        let course_name = Batch_data[i].course_name;

        let submission_array = submission_data.filter(function (obj) {
            return obj.batch_id === batch_id;
        });

        let submission_count = submission_array.length;
        let accepted_array = submission_array.filter(function (obj) {
            return obj.status === 2;
        });
        let accepted_count = accepted_array.length;

        let rejected_array = submission_array.filter(function (obj) {
            return obj.status === 3;
        });
        let rejected_count = rejected_array.length;

        let json = {
            batch_id: batch_id,
            batch_name: batch_name,
            course_code: course_code,
            course_name: course_name,
            submission_count: submission_count,
            accepted_count: accepted_count,
            rejected_count: rejected_count,
            question_count: question_count,
            not_submission_count: not_submission_count,
        }

        status.push(json);
    }

    let faculty_array = [];
    let db4 = mongo.get().collection("user_table")
    faculty_array = await db4.find({ "role": "faculty" }).toArray()
    res.json({ Batch_data: status, faculty_array: faculty_array })
});


router.post('/faculty_count_dashboard', async (req, res) => {

    var status = [];
    let question_count = 0, not_submission_count = 0;

    let db1 = mongo.get().collection("batch_table")
    let db2 = mongo.get().collection("submission_table")
    let db3 = mongo.get().collection("question_table");


    let Batch_data = await db1.find({ faculty_id: req.body.faculty_id }).toArray()
    let submission_data = await db2.find({ faculty_id: req.body.faculty_id }).toArray()

    for (let i = 0; i < Batch_data.length; i++) {
        let batch_id = parseInt(Batch_data[i]._id);
        question_count = await db3.find({ "batch_id": parseInt(batch_id) }).count();
        let submission = await db3.find({ "batch_id": parseInt(batch_id) }).toArray();

        not_submission_count = question_count - submission.length;

        let batch_name = Batch_data[i].batch_name;
        let course_code = Batch_data[i].course_code;
        let course_name = Batch_data[i].course_name;

        let submission_array = submission_data.filter(function (obj) {
            return obj.batch_id === batch_id;
        });

        let submission_count = submission_array.length;
        let accepted_array = submission_array.filter(function (obj) {
            return obj.status === 2;
        });
        let accepted_count = accepted_array.length;

        let rejected_array = submission_array.filter(function (obj) {
            return obj.status === 3;
        });
        let rejected_count = rejected_array.length;

        let json = {
            batch_id: batch_id,
            batch_name: batch_name,
            course_code: course_code,
            course_name: course_name,
            submission_count: submission_count,
            accepted_count: accepted_count,
            rejected_count: rejected_count,
            question_count: question_count,
            not_submission_count: not_submission_count,

        }

        status.push(json);
    }

    console.log(status)
    res.json({ faculty_data: status })
});


module.exports = router;