const express = require('express');
const router = express.Router();


const Login = require('./Login');
const Faculty = require('./Faculty');
const Admin = require('./Admin');
const Student =require('./Student')
const AdminBulk = require('./Admin/bulk');
const FacultyBulk =require('./Faculty/bulk');


router.use('/login', Login);
router.use('/admin', Admin);
router.use('/faculty', Faculty);
router.use('/student',Student);
router.use('/admin/bulk', AdminBulk);
router.use('/faculty/bulk',FacultyBulk);

module.exports = router;