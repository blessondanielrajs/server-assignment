const express = require('express');
const router = express.Router();
const mongo = require('../../mongo');

router.post('/my_batches', async (req, res) => {
    let db1 = mongo.get().collection("user_table");
    let db2 = mongo.get().collection("batch_table");
    let db3 = mongo.get().collection("submission_table");
    let db4 = mongo.get().collection("question_table");

    var data = [];

    let query = await db1.find({ "_id": req.body._id }).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error.t Contact Admin!!" }));

    let batches = query[0].slot;
    for (i = 0; i < batches.length; i++) {
        let json = {}, submission_count = 0, rejected_count = 0, question_count = 0, accepted_count = 0, not_submission_count = 0;
        let batch_id = parseInt(batches[i].batch_id);
        question_count = await db4.find({ "batch_id": parseInt(batch_id) }).count();
        let submission = await db3.find({ "student_id": req.body._id, "batch_id": parseInt(batch_id) }).toArray();
        not_submission_count = question_count - submission.length;
        let batches_info = await db2.find({ "_id": parseInt(batch_id) }).toArray();
        let batch_name = batches_info[0].batch_name;
        let faculty_id = batches_info[0].faculty_id;
        let faculty_name = batches_info[0].faculty_name;
        let course_name = batches_info[0].course_name;
        let course_code = batches_info[0].course_code;
     
        if (submission.length) {
            submission_count = submission.length;
            let accepted_array = submission.filter(function (obj) {
                return obj.status === 2;
            });
            accepted_count = accepted_array.length;

            let rejected_array = submission.filter(function (obj) {
                return obj.status === 3;
            });
            rejected_count = rejected_array.length;
        }

        json = {
            batch_id: batch_id,
            batch_name: batch_name,
            faculty_id: faculty_id,
            faculty_name: faculty_name,
            course_name: course_name,
            course_code: course_code,
            question_count: question_count,
            submission_count: submission_count,
            not_submission_count: not_submission_count,
            accepted_count: accepted_count,
            rejected_count: rejected_count,
        }

        data.push(json);

    }
    //console.log(data)
  

    res.json({ data: data })
});


router.post('/question', async (req, res) => {
    let db1 = mongo.get().collection("question_table");
    let db2 = mongo.get().collection("submission_table");
    let question = await db1.find({ "batch_id": parseInt(req.body.batch_id) }).toArray()
    let statusArray = await db2.find({ student_id: parseInt(req.body.student_id), "batch_id": parseInt(req.body.batch_id) }).toArray()
    for (i = 0; i < question.length; i++) {

        let x = statusArray.findIndex(item => item.question_id === question[i]._id);
        if (x >= 0) {
            question[i].status = statusArray[x].status;
            question[i].mark = statusArray[x].mark;
            question[i].msg = statusArray[x].msg;
        }
        else {
            question[i].status = -1;
            question[i].mark = 0;
            question[i].msg = "No";
        }



    }
    console.log(question)

    res.json({ question: question })
});

router.post('/submission', async (req, res) => {
    let db1 = mongo.get().collection("submission_table");
    let new_id = 1;
    let max = await db1.find().sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    let _id = req.body._id + req.body.batch_id + req.body.question_id;
    console.log(_id);
    let json = {
        "_id": parseInt(_id),
        "faculty_id": parseInt(req.body.faculty_id),
        "batch_id": parseInt(req.body.batch_id),
        "batch_name": req.body.batch_name,
        "question_id": parseInt(req.body.question_id),
        "course_code": req.body.course_code,
        "course_name": req.body.course_name,
        "question_name": req.body.question_name,
        "student_id": req.body._id,
        "student_name": req.body.student_name,
        "answer": req.body.answer,
        "submission_date": parseInt(Date.now() / 1000),
        "status": parseInt(1),
        "mark": parseInt(0),
        "msg": ""
    }

    const query = { _id: _id };
    const update = { $set: json };
    const options = { upsert: true };
    let insert = await db1.updateOne(query, update, options);

    if (insert.acknowledged) {
        res.json({ status: 1 });
    }
    else {
        res.json({ status: 0 });
    }

});

router.post('/batch_id', async (req, res) => {
    let db1 = mongo.get().collection("batch_table");
    let batch_id = await db1.find({ "batch_name": req.body.batch_name }).toArray()

    console.log(batch_id[0]._id)

    res.json({ batch_id: batch_id[0]._id })
});


module.exports = router;